package idp.moviebooking.handler;

import idp.moviebooking.model.MovieDto;
import idp.moviebooking.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static idp.moviebooking.model.Constants.SORT_BY_TITLE;

@Controller
@RequestMapping("/api/v1/movies")
public class MovieHandler {
    @Autowired
    private MovieService movieService;

    @GetMapping("/info")
    public ResponseEntity<List<MovieDto>> getMovies(@RequestParam Integer perPage,
                                                    @RequestParam Integer page) {
        Pageable pageable = PageRequest.of(page, perPage, Sort.by(SORT_BY_TITLE));

        return ResponseEntity.ok(movieService.listMoviesPageable(pageable));
    }
}
