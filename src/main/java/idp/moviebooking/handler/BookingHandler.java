package idp.moviebooking.handler;

import idp.moviebooking.model.Constants;
import idp.moviebooking.service.BookingService;
import idp.moviebooking.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/v1/booking")
public class BookingHandler {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private MovieService movieService;

    @PostMapping("/{user}/booking/{movieId}")
    public ResponseEntity<Long> addBooking(@PathVariable String user,
                                           @PathVariable Long movieId,
                                           @RequestBody List<String> reservedSeats) {
        return ResponseEntity.ok(bookingService.addBooking(movieId, reservedSeats, user));
    }

    @GetMapping("/{user}/bookings/{bookingId}")
    public ResponseEntity<String> getBookingById(@PathVariable String user,
                                                 @PathVariable Long bookingId) {
        return ResponseEntity.ok(bookingService.getBooking(bookingId));
    }

    @GetMapping("/{user}/bookings")
    public ResponseEntity<List<String>> getBookingsByUser(@PathVariable String user) {
        return ResponseEntity.ok(bookingService.getBookingsByUser(user));
    }

    @DeleteMapping("/{user}/bookings/{bookingId}")
    public ResponseEntity<String> deleteBooking(@PathVariable String user,
                                                @PathVariable Long bookingId) {
        bookingService.deleteBooking(bookingId);

        return ResponseEntity.ok(Constants.BOOKING_DELETED_MESSAGE);
    }
}
