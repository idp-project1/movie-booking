package idp.moviebooking.service;

import idp.moviebooking.entity.MovieEntity;
import idp.moviebooking.model.MovieDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MovieService {

    List<MovieDto> listMoviesPageable(Pageable pageable);

    void deleteSeat(Long movie_id, String seat);

    void addSeat(Long movieId, String seat);

    Boolean seatExists(MovieEntity movieEntity, String seat);

    MovieEntity getMovieById(Long movieId);
}