package idp.moviebooking.service;

import java.util.List;

public interface BookingService {
    Long addBooking(Long movieId, List<String> reservedSeats, String user);

    void deleteBooking(Long bookingId);

    List<String> getBookingsByUser(String user);

    String getBooking(Long bookingId);
}
