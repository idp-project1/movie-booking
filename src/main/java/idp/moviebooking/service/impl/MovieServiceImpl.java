package idp.moviebooking.service.impl;

import idp.moviebooking.entity.MovieEntity;
import idp.moviebooking.exceptions.MovieNotFoundException;
import idp.moviebooking.model.Constants;
import idp.moviebooking.model.MovieDto;
import idp.moviebooking.repository.MovieRepository;
import idp.moviebooking.service.MovieService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<MovieDto> listMoviesPageable(Pageable pageable) {
        return movieRepository.findAll(pageable).toList()
                .stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void deleteSeat(Long movieId, String seat) {
        //noinspection OptionalGetWithoutIsPresent
        MovieEntity movieEntity = movieRepository.findById(movieId).get();
        System.out.println(movieEntity.getEmptySeats());

        List<String> temporarySeats = new ArrayList<>(movieEntity.getEmptySeats());
        temporarySeats.remove(seat);

        MovieDto movieDto = convertToDto(movieEntity);

        movieDto.setEmptySeats(temporarySeats);
        movieRepository.save(convertToEntity(movieDto));
    }

    @Override
    public void addSeat(Long movieId, String seat) {
        //noinspection OptionalGetWithoutIsPresent
        MovieEntity movieEntity = movieRepository.findById(movieId).get();
        List<String> temporarySeats = new ArrayList<>(movieEntity.getEmptySeats());

        temporarySeats.add(seat);
        MovieDto movieDto = convertToDto(movieEntity);

        movieDto.setEmptySeats(temporarySeats);
        movieRepository.save(convertToEntity(movieDto));
    }

    @Override
    public Boolean seatExists(MovieEntity movieEntity, String seat) {
        if (!isValidSeatFormat(seat)) {
            return false;
        }
        boolean expectedConstraints = seat.charAt(0) >= Constants.MIN_ROW && seat.charAt(0) <= Constants.MAX_ROW &&
                Integer.parseInt(String.valueOf(seat.charAt(1))) < Constants.MAX_SEAT_NUMBER;

        return expectedConstraints && movieEntity.getEmptySeats().contains(seat);
    }

    @Override
    public MovieEntity getMovieById(Long movieId) {
        Optional<MovieEntity> movieEntity = movieRepository.findById(movieId);

        if (movieEntity.isEmpty()) {
            throw new MovieNotFoundException();
        }

        return movieEntity.get();
    }

    private Boolean isValidSeatFormat(String seat) {
        return Character.isLetter(seat.charAt(0)) &&
                seat.length() == Constants.SEAT_LENGTH &&
                Character.isDigit(seat.charAt(1));
    }

    private MovieDto convertToDto(MovieEntity movieEntity) {
        return modelMapper.map(movieEntity, MovieDto.class);
    }

    private MovieEntity convertToEntity(MovieDto movieDto) {
        return modelMapper.map(movieDto, MovieEntity.class);
    }
}