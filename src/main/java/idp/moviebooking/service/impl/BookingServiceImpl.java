package idp.moviebooking.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import idp.moviebooking.entity.BookingEntity;
import idp.moviebooking.entity.MovieEntity;
import idp.moviebooking.exceptions.BookingNotFoundException;
import idp.moviebooking.exceptions.MovieNotFoundException;
import idp.moviebooking.exceptions.SeatUnavailableException;
import idp.moviebooking.model.BookingDto;
import idp.moviebooking.repository.BookingRepository;
import idp.moviebooking.service.BookingService;
import idp.moviebooking.service.MovieService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private MovieService movieService;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ObjectMapper objectMapper;

    public Long addBooking(Long movieId, List<String> reservedSeats, String user) {
        MovieEntity movieEntity = movieService.getMovieById(movieId);

        verifyDataInsertion(movieEntity, movieId, reservedSeats);
        reservedSeats.forEach(seat -> movieService.deleteSeat(movieId, seat));

        BookingEntity bookingEntity = BookingEntity.builder()
                .movieId(movieId)
                .seats(reservedSeats)
                .user(user).build();

        bookingEntity = bookingRepository.save(bookingEntity);

        return bookingEntity.getId();
    }

    @Override
    public void deleteBooking(Long bookingId) {
        Optional<BookingEntity> bookingEntity = bookingRepository.findById(bookingId);

        if (bookingEntity.isPresent()) {
            Long movieId = bookingEntity.get().getMovieId();

            bookingEntity.get().getReservedSeats().forEach(seat -> movieService.addSeat(movieId, seat));
            bookingRepository.deleteById(bookingId);
        }
    }

    @Override
    public List<String> getBookingsByUser(String user) {
        List<BookingEntity> bookingEntities = bookingRepository.findAllByUser(user);

        if (bookingEntities.isEmpty()) {
            return Collections.emptyList();
        }
        return bookingEntities.stream().map(this::composeResponse).collect(Collectors.toList());
    }

    @Override
    public String getBooking(Long bookingId) {
        Optional<BookingEntity> bookingEntity = bookingRepository.findById(bookingId);

        if (bookingEntity.isEmpty()) {
            throw new BookingNotFoundException();
        }

        return composeResponse(bookingEntity.get());
    }


    private BookingDto convertToDto(BookingEntity bookingEntity) {
        return modelMapper.map(bookingEntity, BookingDto.class);
    }

    private BookingEntity convertToEntity(BookingDto bookingDto) {
        return modelMapper.map(bookingDto, BookingEntity.class);
    }

    private void verifyDataInsertion(MovieEntity movieEntity, Long movieId, List<String> reservedSeats) {
        if (movieEntity == null) {
            throw new MovieNotFoundException();
        }

        boolean hasUnavailableSeats = reservedSeats
                .stream()
                .map(seat -> movieService.seatExists(movieEntity, seat))
                .collect(Collectors.toList())
                .contains(Boolean.FALSE);

        if (hasUnavailableSeats) {
            throw new SeatUnavailableException();
        }
    }

    private String composeResponse(BookingEntity bookingEntity) {
        MovieEntity movieEntity = movieService.getMovieById(bookingEntity.getMovieId());

        return bookingEntity.toString() + movieEntity.toString();
    }
}
