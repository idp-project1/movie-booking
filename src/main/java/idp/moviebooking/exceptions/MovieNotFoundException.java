package idp.moviebooking.exceptions;

import static idp.moviebooking.model.Constants.MOVIE_NOT_FOUND_EXCEPTION_MESSAGE;

public class MovieNotFoundException extends RuntimeException {
    public MovieNotFoundException() {
        super(MOVIE_NOT_FOUND_EXCEPTION_MESSAGE);
    }
}
