package idp.moviebooking.exceptions;

import static idp.moviebooking.model.Constants.BOOKING_NOT_FOUND_EXCEPTION_MESSAGE;

public class BookingNotFoundException extends RuntimeException {
    public BookingNotFoundException() {
        super(BOOKING_NOT_FOUND_EXCEPTION_MESSAGE);
    }
}
