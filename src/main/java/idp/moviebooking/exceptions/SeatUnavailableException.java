package idp.moviebooking.exceptions;

import static idp.moviebooking.model.Constants.SEAT_UNAVAILABLE__EXCEPTION_MESSAGE;

public class SeatUnavailableException extends RuntimeException {

    public SeatUnavailableException() {
        super(SEAT_UNAVAILABLE__EXCEPTION_MESSAGE);
    }
}