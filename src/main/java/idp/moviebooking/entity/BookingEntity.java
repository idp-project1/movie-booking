package idp.moviebooking.entity;

import idp.moviebooking.model.StringListConverter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "bookings")
public class BookingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "tenant")
    private String user;
    @Column(name = "reserved_seats")
    @Convert(converter = StringListConverter.class)
    private List<String> reservedSeats;
    @Column(name = "movie_id")
    private Long movieId;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final BookingEntity bookingEntity = new BookingEntity();

        private Builder() {
        }

        public BookingEntity.Builder id(Long id) {
            bookingEntity.id = id;
            return this;
        }

        public BookingEntity.Builder user(String user) {
            bookingEntity.user = user;
            return this;
        }

        public BookingEntity.Builder seats(List<String> seats) {
            bookingEntity.reservedSeats = seats;
            return this;
        }

        public BookingEntity.Builder movieId(Long movieId) {
            bookingEntity.movieId = movieId;
            return this;
        }

        public BookingEntity build() {
            return bookingEntity;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<String> getReservedSeats() {
        return reservedSeats;
    }

    public void setReservedSeats(List<String> reservedSeats) {
        this.reservedSeats = reservedSeats;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingEntity that = (BookingEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(user, that.user) &&
                Objects.equals(reservedSeats, that.reservedSeats) &&
                Objects.equals(movieId, that.movieId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, reservedSeats, movieId);
    }

    @Override
    public String toString() {
        return "{" +
                "\n\tid: " + id +
                "\n\t user: " + user +
                "\n\t reservedSeats: " + reservedSeats;
    }
}
