package idp.moviebooking.entity;

import idp.moviebooking.model.StringListConverter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "movies")
public class MovieEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "room", nullable = false)
    private String room;
    @Column(name = "time", nullable = false)
    private Date time;
    @Column(name = "empty_seats")
    @Convert(converter = StringListConverter.class)
    private List<String> emptySeats;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final MovieEntity movieEntity = new MovieEntity();

        private Builder() {
        }

        public Builder id(Long id) {
            movieEntity.id = id;
            return this;
        }

        public Builder title(String title) {
            movieEntity.title = title;
            return this;
        }

        public Builder room(String room) {
            movieEntity.room = room;
            return this;
        }

        public Builder time(Date time) {
            movieEntity.time = time;
            return this;
        }

        public Builder emptySeats(List<String> emptySeats) {
            movieEntity.emptySeats = emptySeats;
            return this;
        }

        public MovieEntity build() {
            return movieEntity;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public List<String> getEmptySeats() {
        return emptySeats;
    }

    public void setEmptySeats(List<String> emptySeats) {
        this.emptySeats = emptySeats;
    }

    @Override
    public String toString() {
        return "\n\t{\n" +
                "\t\tid: " + id +
                "\n\t\t title: " + title +
                "\n\t\t room: " + room +
                "\n\t\t time: " + time +
                "\n\t}\n" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieEntity that = (MovieEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(title, that.title) &&
                Objects.equals(room, that.room) && Objects.equals(time, that.time) &&
                Objects.equals(emptySeats, that.emptySeats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, room, time, emptySeats);
    }
}
