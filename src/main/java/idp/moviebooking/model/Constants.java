package idp.moviebooking.model;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Constants {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static final Integer MAX_SEAT_NUMBER = 10;
    public static final Character MAX_ROW = 'E';
    public static final Character MIN_ROW = 'A';
    public static final Integer SEAT_LENGTH = 2;
    public static final String BOOKING_DELETED_MESSAGE = "Booking deleted.";
    public static final String SORT_BY_TITLE = "title";
    public static final String SORT_BY_ID= "id";

    public static final String MOVIE_NOT_FOUND_EXCEPTION_MESSAGE = "Movie not found.";
    public static final String BOOKING_NOT_FOUND_EXCEPTION_MESSAGE = "Booking not found.";
    public static final String SEAT_UNAVAILABLE__EXCEPTION_MESSAGE = "Seat unavailable.";
}
