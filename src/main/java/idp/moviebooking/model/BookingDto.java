package idp.moviebooking.model;

import java.util.List;
import java.util.Objects;

public class BookingDto {
    private Long id;
    private String user;
    private List<String> seats;
    private Long movieId;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final BookingDto bookingDto = new BookingDto();

        private Builder() {
        }

        public BookingDto.Builder user(Long id) {
            bookingDto.id = id;
            return this;
        }

        public BookingDto.Builder user(String user) {
            bookingDto.user = user;
            return this;
        }

        public BookingDto.Builder seats(List<String> seats) {
            bookingDto.seats = seats;
            return this;
        }

        public BookingDto.Builder movieId(Long movie) {
            bookingDto.movieId = movie;
            return this;
        }

        public BookingDto build() {
            return bookingDto;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<String> getSeats() {
        return seats;
    }

    public void setSeats(List<String> seats) {
        this.seats = seats;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingDto that = (BookingDto) o;
        return Objects.equals(id, that.id) && Objects.equals(user, that.user) && Objects.equals(seats, that.seats) && Objects.equals(movieId, that.movieId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, seats, movieId);
    }

    @Override
    public String toString() {
        return "BookingDto{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", seats='" + seats + '\'' +
                ", movieId='" + movieId + '\'' +
                '}';
    }
}
