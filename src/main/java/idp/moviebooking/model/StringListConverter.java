package idp.moviebooking.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Collections.*;

@Converter
public class StringListConverter implements AttributeConverter<List<String>, String> {
    private static final String SPLIT_CHAR = "-";

    @Override
    public String convertToDatabaseColumn(List<String> stringList) {
        if (stringList != null) {
            return stringList.stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining(SPLIT_CHAR, "", ""));
        }
        return "";
    }

    @Override
    public List<String> convertToEntityAttribute(String string) {
        return string != null ? Arrays.asList(string.split(SPLIT_CHAR)) : emptyList();
    }
}