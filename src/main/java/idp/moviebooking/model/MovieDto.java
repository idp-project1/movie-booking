package idp.moviebooking.model;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class MovieDto {
    private Long id;
    private String title;
    private String room;
    private Date time;
    private List<String> emptySeats;

    public static MovieDto.Builder builder() {
        return new MovieDto.Builder();
    }

    public static class Builder {
        private final MovieDto movieDto = new MovieDto();

        private Builder() {
        }

        public Builder id(Long id) {
            movieDto.id = id;
            return this;
        }

        public Builder title(String title) {
            movieDto.title = title;
            return this;
        }

        public Builder room(String room) {
            movieDto.room = room;
            return this;
        }

        public Builder time(Date time) {
            movieDto.time = time;
            return this;
        }

        public Builder emptySeats(List<String> emptySeats) {
            movieDto.emptySeats = emptySeats;
            return this;
        }

        public MovieDto build() {
            return movieDto;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public List<String> getEmptySeats() {
        return emptySeats;
    }

    public void setEmptySeats(List<String> emptySeats) {
        this.emptySeats = emptySeats;
    }

    @Override
    public String toString() {
        return "MovieEntity{" +
                "id= " + id + '\'' +
                ", title='" + title + '\'' +
                ", room='" + room + '\'' +
                ", time=" + time +
                ", emptySeats=" + emptySeats +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieDto that = (MovieDto) o;
        return Objects.equals(id, that.id) && Objects.equals(title, that.title) && Objects.equals(room, that.room) && Objects.equals(time, that.time) && Objects.equals(emptySeats, that.emptySeats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, room, time, emptySeats);
    }
}
