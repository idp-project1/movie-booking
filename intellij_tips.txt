Useful tips:
0. Shortcuts in Intellij
Ctrl + Alt + o -> remove unused imports
Ctrl + Alt + l -> auto indent
Ctrl + d -> duplicate current line
Alt + Enter on error/warning -> get solutions

1. When extracting the date and hour

--date
Locale locale = new Locale("fr", "FR");
DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
String date = dateFormat.format(new Date());
System.out.print(date);

--hour
Locale locale = new Locale("fr", "FR");
DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale);
String date = dateFormat.format(new Date());
System.out.print(date);