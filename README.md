# MOVIE BOOKING SERVICE

### Business Logic component

[![build status](https://gitlab.com/idp-project1/movie-booking/badges/master/pipeline.svg)](https://gitlab.com/idp-project1/movie-booking/commits/master)

(C) Copyright 2021

The team:

- Călătoaie Iulia-Adriana 343 C1

- Georgescu Alin-Andrei 342 C3

- Millio Anca 342 C4

- Negru Bogdan-Cristian 342 C3

This is a cinema movie booking web application.

The app functionalities are:

- GET "/api/v1/movies/info?perPage=10&page=0"
  - retrieve information about all available movies
  - return 200 OK and a list of movies

- POST "/api/v1/booking/{user}/booking/{movieId}"
  - book one or more seats
  - return 200 OK and the booking id or 404 Not Found with 2 possible messages
  (Movie not found. / Seat unavailable.)

- GET "/api/v1/booking/{user}/bookings/{bookingId}
  - get a booking information
  - return 200 OK and a booking info or 404 Not Found with a message
  (Booking not found.)

- GET "/api/v1/booking/{user}/bookings"
  - get a user's bookings
  - return 200 OK and a list of bookings' information

- DELETE "/api/v1/booking/{user}/bookings/{bookingId}
  - delete a user's bookings
  - return 200 OK
